from flask import Flask, request, Response, jsonify
from configuration import Configuration
from models import database, User, UserRole
from email.utils import parseaddr
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, create_refresh_token, get_jwt, get_jwt_identity
from sqlalchemy import and_
import re
from roleCheck import roleCheck

application = Flask(__name__)
application.config.from_object(Configuration)

def validateJMBG(jmbg):
    if len(jmbg) == 13:
        dd = jmbg[0] + jmbg[1]
        dd = int(dd)

        mm = jmbg[2] + jmbg[3]
        mm = int(mm)

        rr = jmbg[7] + jmbg[8]
        rr = int(rr)

        # rr < 70 - only Serbian residents
        if dd < 1 or dd > 31 or mm < 1 or mm > 12 or rr < 70:
            return False

        sum = 0

        for i in range(6):
            sum += (7-i) * (int(jmbg[i]) + int(jmbg[6+i]))
            remainder = sum % 11
            res = 11 - remainder
        
        if remainder == 1 or (remainder == 0 and (int(jmbg[12]) != 0)) or (res != int(jmbg[12])):
            return False

        return True
    else:
        return False


@application.route('/register', methods=['POST'])
def register():
    jmbg = request.json.get('jmbg', '')
    email = request.json.get('email', '')
    password = request.json.get('password', '')
    forename = request.json.get('forename', '')
    surname = request.json.get('surname', '')

    if len(jmbg) == 0:
        return jsonify(message = 'Field jmbg is missing.'), 400

    if len(forename) == 0:
        return jsonify(message = 'Field forename is missing.'), 400

    if len(surname) == 0:
        return jsonify(message = 'Field surname is missing.'), 400

    if len(email) == 0:
        return jsonify(message = 'Field email is missing.'), 400

    if len(password) == 0:
        return jsonify(message = 'Field password is missing.'), 400

        
    jmbgValid = validateJMBG(jmbg)

    if not jmbgValid:
        return jsonify(message = 'Invalid jmbg.'), 400

    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    emailValid = re.match(regex, email)

    if not emailValid:
        return jsonify(message = 'Invalid email.'), 400

    passwordValid = re.match(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$", password)

    if not passwordValid:
        return jsonify(message = 'Invalid password.'), 400

    emailExists = User.query.filter(User.email == email).first()

    if emailExists:
        return jsonify(message = 'Email already exists.'), 400
    
    
    user = User(jmbg=jmbg, email=email, password=password, forename=forename, surname=surname)

    database.session.add(user)
    database.session.commit()

    userRole = UserRole(userID=user.id, roleID=2)
    database.session.add(userRole)
    database.session.commit()

    return Response('Registration successful', status=200)


jwt = JWTManager(application)


@application.route('/login', methods=['POST'])
def login():
    email = request.json.get('email', '')
    password = request.json.get('password', '')

    emailEmpty = len(email) == 0
    passwordEmpty = len(password) == 0

    if emailEmpty:
        return jsonify(message = 'Field email is missing.'), 400

    if passwordEmpty:
        return jsonify(message = 'Field password is missing.'), 400

    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    emailValid = re.match(regex, email)

    if not emailValid:
        return jsonify(message = 'Invalid email.'), 400

    user = User.query.filter(and_(User.email == email, User.password == password)).first()

    if (not user):
        return jsonify(message = 'Invalid credentials.'), 400

    additionalClaims = {
        'forename': str(user.forename),
        'surname': str(user.surname),
        'jmbg': str(user.jmbg),
        'roles': [str(role) for role in user.roles]
    }

    accessToken = create_access_token(identity=user.email, additional_claims=additionalClaims)
    refreshToken = create_refresh_token(identity=user.email, additional_claims=additionalClaims)

    return jsonify(accessToken=accessToken, refreshToken=refreshToken)

@application.route('/')
def index():
    return 'Nothing interesting here...'

@application.route('/refresh', methods = ['POST'])
@jwt_required( refresh = True )
def refresh():
    identity = get_jwt_identity()
    refreshClaims = get_jwt()

    additionalClaims = {
        'forename': refreshClaims['forename'],
        'surname': refreshClaims['surname'],
        'jmbg': refreshClaims['jmbg'],  
        'roles': refreshClaims['roles']
    }

    return jsonify(accessToken = create_access_token(identity = identity, additional_claims = additionalClaims))

@application.route('/delete', methods = ['POST'])
@roleCheck(role = 'admin')
def delete():
    email = request.json.get('email', '')

    emailEmpty = len(email) == 0

    if emailEmpty:
        return jsonify(message = 'Field email is missing.'), 400

    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    emailValid = re.match(regex, email)

    if not emailValid:
        return jsonify(message = 'Invalid email.'), 400

    user = User.query.filter(User.email == email).first()

    if (not user):
        return jsonify(message = 'Unknown user.'), 400

    application.logger.info(user.id)

    # UserRole.query.filter(UserRole.userID == user.id).delete()
    # User.query.filter(User.email == email).delete()
    database.session.delete(user)
    database.session.commit()

    return Response('', status = 200)


if (__name__ == '__main__'):
    database.init_app(application)
    application.run(debug=True, host='0.0.0.0', port=80)
