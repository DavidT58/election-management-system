from logging import debug
from flask import Flask, app, json, Request, Response, jsonify
from flask.globals import request
from configuration import Configuration
from models import ElectionParticipant, database, Participant, Election, Vote
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, create_refresh_token, get_jwt, get_jwt_identity
from roleCheck import roleCheck
from datetime import datetime
import re, math
from sqlalchemy import select
from collections import Counter
from voting import apportionment

application = Flask(__name__)
application.config.from_object(Configuration)

jwt = JWTManager(application)

@application.route("/createParticipant", methods = ["POST"])
@roleCheck(role = 'admin')
def createParticipant():
    name = request.json.get('name', '')
    individualRequest = request.json.get('individual', '')

    if len(name) == 0:
        return jsonify(message = 'Field name is missing.'), 400

    if individualRequest is None:
        return jsonify(message= 'Field individual is missing.'), 400

    # application.logger.debug(individualRequest == True)

    individual = False

    if individualRequest == True and type(individualRequest) == bool:
        individual = True
    elif type(individualRequest) != bool:
        return jsonify(message= 'Field individual is missing.'), 400
    

    
    participant = Participant(name = name, individual = individual)

    database.session.add(participant)
    database.session.commit()

    id = participant.id
    
    return jsonify(id = id)


@application.route("/getParticipants", methods = ["GET"])
@roleCheck(role = 'admin')
def getParticipants():

    participants = Participant.query.all()

    ret = []

    for participant in participants:
        # application.logger.debug(str(participant.id) + " " + participant.name + " " + str(participant.individual))
        i = {
            "id": participant.id,
            "name": participant.name,
            "individual": participant.individual 
        }
        ret.append(i)

    return jsonify(participants = ret)

def validateTimeFormat(time):
    
    try:
        datetime.fromisoformat(time)
    except:
        return False
    return True

def electionExists(startArg, endArg):
    elections = Election.query.all()
    
    regex = r'\+\d{4}'

    start = str(startArg)
    end = str(endArg)

    val = re.findall(regex, start)

    # application.logger.debug("Time debug:" + str(val))


    for election in elections:
        if election.start <= endArg and startArg <= election.end:
            return True

    return False

@application.route("/createElection", methods = ["POST"])
@roleCheck(role = 'admin')
def createElection():

    startTimeRequest = request.json.get("start")
    endTimeRequest = request.json.get("end")
    individualRequest = request.json.get("individual", "")
    participantsRequest = request.json.get("participants")

    if startTimeRequest is None or len(startTimeRequest) == 0:
        return jsonify(message = "Field start is missing."), 400

    if endTimeRequest is None or len(endTimeRequest) == 0:
        return jsonify(message = "Field end is missing."), 400
    
    if individualRequest is None or len(str(individualRequest)) == 0:
        return jsonify(message = "Field individual is missing."), 400

    if participantsRequest is None or len(str(participantsRequest)) == 0:
        return jsonify(message = "Field participants is missing."), 400


    if (not validateTimeFormat(startTimeRequest) or 
        not validateTimeFormat(endTimeRequest)):
        return jsonify(message = "Invalid date and time."), 400

    startTime = datetime.fromisoformat(startTimeRequest)
    endTime = datetime.fromisoformat(endTimeRequest)

    if startTime > endTime or electionExists(startTime, endTime):
        return jsonify(message = "Invalid date and time."), 400

    participants = []

    for participantID in participantsRequest:
        participant = Participant.query.get(participantID)
        if not participant or participant.individual != individualRequest:
            return jsonify(message = "Invalid participant."), 400
        participants.append(participant)
    
    if len(participants) < 2:
        return jsonify(message = "Invalid participant."), 400

    election = Election(start = startTime, end = endTime, individual = individualRequest)

    database.session.add(election)
    database.session.commit()

    cnt = 1
    for participant in participants:
        electionParticipant = ElectionParticipant(participantid = participant.id, electionid = election.id, pollNumber = cnt)
        cnt += 1
        database.session.add(electionParticipant)
        database.session.commit()
    
    # application.logger.debug(participantsRequest)

    r = ElectionParticipant.query.filter_by(electionid = election.id).all()

    ret = []
    for p in r:
        ret.append(p.pollNumber)

    return jsonify(pollNumbers = ret)


@application.route("/getElections", methods = ["GET"])
@roleCheck(role = 'admin')
def getElections():

    elections = Election.query.all()

    # application.logger.debug(elections)

    ret = []

    for election in elections:
        participants = ElectionParticipant.query.filter_by(electionid = election.id).all()
        t = []
        for participant in participants:
            p = {
                "id": participant.participantid,
                "name": Participant.query.filter_by(id = participant.participantid).all()[0].name
            }
            t.append(p)
        i = {
            "id": int(election.id),
            "start": str(election.start),
            "end": str(election.end),
            "individual": bool(election.individual),
            "participants": t
        }
        ret.append(i)

    return jsonify(elections = ret)

def countPresidentVotes(electionID):
    ids = []
    votes = Vote.query.filter_by(electionid = electionID).filter_by(valid = 1).all()

    for vote in votes:
        ids.append(vote.pollNumber)

    results = Counter(ids)

    dict = {}

    for (key,value) in results.items():
        dict[key] = float("{:.2f}".format(value / len(ids)))

    return dict

def countParliamentaryVotes(electionID):

    # 250 seats
    # census 5%
    
    # apportionment.dhondt()

    election = Election.query.filter_by(id = electionID).all()
    votes = Vote.query.filter_by(electionid = electionID, valid = 1).all()

    for vote in votes:
        application.logger.debug(f'ID: {vote.id} UUID: {vote.uuid} PollNumber: {vote.pollNumber} VoterJMBG{vote.voterid}')

    totalVotes = len(votes)

    ids = []

    for vote in votes:
        ids.append(vote.pollNumber)

    ctr = Counter(ids)

    dict = {}

    application.logger.debug(f'Threshold: len(ids) * 0.05: {len(ids) * 0.05}')

    for (key,value) in ctr.items():
        application.logger.debug(f'value: {value} value/len(ids): {value / len(ids)}')
        dict[key] = value if (value > (len(ids) * 0.05)) else 0

    seats = list(ctr.values())

    application.logger.debug(f'before: {seats}')

    for i in range(len(seats)):
        if seats[i] < (len(ids) * 0.05):
            seats[i] = 0

    application.logger.debug(f'after: {seats}')

    application.logger.debug(f'Dict: {dict}')

    results = apportionment.dhondt(seats, 250)

    return results

@application.route("/getResults", methods = ["GET"])
@roleCheck(role = 'admin')
def getResults():

    id = request.args.get("id", '')
    
    if id is None:
        return jsonify(message = "Field id is missing."), 400

    if id == '':
        return jsonify(message = "Field id is missing."), 400

    election = Election.query.get(id)

    if election is None:
        return jsonify(message = "Election does not exist."), 400

    if election.end > datetime.now():
        return jsonify(message = "Election is ongoing."), 400

    electionParticipants = ElectionParticipant.query.filter_by(electionid = id).with_entities(ElectionParticipant.participantid, ElectionParticipant.pollNumber).all()

    # True - president, False - parliamentary
    electionType = Election.query.get(id).individual
    # application.logger.debug(electionType)

    results = countPresidentVotes(id) if electionType else countParliamentaryVotes(id)

    participants = []


    for participant in electionParticipants:
        p = {}
        p['pollNumber'] = participant[1]
        p['name'] = Participant.query.get(participant[0]).name
        application.logger.debug("Results: {}".format(results))
        application.logger.debug("Participant: {}".format(participant))
        if results and electionType:
            p['result'] = results[participant[1]]
        # application.logger.debug(results[participant[1]])
        if results and not electionType:
            p['result'] = results[participant[1] - 1]
        participants.append(p)

    invalidVotes = []

    votes = Vote.query.filter_by(valid = 0, electionid = id).all()

    for vote in votes:
        v = {}
        v['electionOfficialJmbg'] = vote.voterid
        v['ballotGuid'] = vote.uuid
        v['pollNumber'] = vote.pollNumber
        v['reason'] = vote.note
        invalidVotes.append(v)

    
    return jsonify(participants = participants, invalidVotes = invalidVotes)

if (__name__ == '__main__'):
    database.init_app(application)
    application.run(debug=True, host='0.0.0.0', port=80)