from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import backref

database = SQLAlchemy()

class Participant(database.Model):
    __tablename__ = "participant"

    id = database.Column(database.Integer, primary_key = True)
    name = database.Column(database.String(256), nullable = False)
    individual = database.Column(database.Boolean, nullable = False)

class Election(database.Model):
    __tablename__ = "election"

    id = database.Column(database.Integer, primary_key = True)
    start = database.Column(database.DateTime, nullable = False)
    end = database.Column(database.DateTime, nullable = False)
    individual = database.Column(database.Boolean, nullable = False)

class Vote(database.Model):
    __tablename__ = "vote"

    id = database.Column(database.Integer, primary_key = True)
    uuid = database.Column(database.String(40), nullable = False)
    pollNumber = database.Column(database.Integer, nullable = False)
    valid = database.Column(database.Boolean, nullable = False)
    note = database.Column(database.String(50), nullable = False)
    electionid = database.Column(database.Integer, database.ForeignKey("election.id"), nullable = False)
    voterid = database.Column(database.String(13), nullable = False)


class ElectionParticipant(database.Model):
    __tablename__ = "electionparticipant"

    id = database.Column(database.Integer, primary_key = True)
    participantid = database.Column(database.Integer, database.ForeignKey("participant.id"), nullable = False)
    electionid = database.Column(database.Integer, database.ForeignKey("election.id"), nullable = False)
    pollNumber = database.Column(database.Integer, nullable = False)
