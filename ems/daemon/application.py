from os import dup
from configuration import Configuration
from models import ElectionParticipant, Participant, Election, Vote
from database import Session
from flask import Flask
# from datetime import datetime, timedelta
from redis import Redis
from datetime import datetime, timedelta
import time
import threading
import sys
import json
import logging

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

session = Session()


def checkVotes():
    with Redis(host = Configuration.REDIS_HOST) as redis:

        subscription = redis.pubsub()
        subscription.subscribe(Configuration.REDIS_VOTE_QUEUE)

        for message in subscription.listen():

            if message['type'] == 'message':

                logging.debug(message)

                data = json.loads(message['data'])

                logging.debug(data)

                id = data.get('id')
                pollNumber = data.get('pollNumber')
                voterJMBG = data.get('voterJMBG')

                elections = session.query(Election).all()
                session.close()

                currentElection = None;
                currentTime = datetime.now().isoformat()

                for election in elections:
                    if election.start.isoformat() <= currentTime and currentTime <= election.end.isoformat():
                        currentElection = election
                        break
                
                if currentElection:
                    # logging.debug(f'Current election: {currentElection.id} {currentElection.start} {currentElection.end} {currentElection.individual}')

                    duplicateBallot = session.query(Vote).filter_by(uuid = id, electionid = currentElection.id).first()

                    pollNumberCheck = session.query(ElectionParticipant).filter_by(electionid = currentElection.id, pollNumber = pollNumber).first()

                    if not pollNumberCheck:
                        logging.debug(f'Invalid poll number')
                        vote = Vote(uuid = id, pollNumber = pollNumber, valid = False, note = 'Invalid poll number.', electionid = currentElection.id, voterid = voterJMBG)
                    elif duplicateBallot:
                        logging.debug(f'Duplicate ballot {duplicateBallot.id} {duplicateBallot.uuid} {duplicateBallot.pollNumber}')
                        vote = Vote(uuid = id, pollNumber = pollNumber, valid = False, note = 'Duplicate ballot.', electionid = currentElection.id, voterid = voterJMBG)
                    else:
                        logging.debug(f'Valid vote')
                        vote = Vote(uuid = id, pollNumber = pollNumber, valid = True, note = '', electionid = currentElection.id, voterid = voterJMBG)
                        
                    session.add(vote)
                    session.commit()

t1 = threading.Thread(target=checkVotes)
t1.start()
