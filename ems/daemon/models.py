from configuration import Configuration
from sqlalchemy import Column, Integer, String, Boolean, DateTime, ForeignKey
from database import Base


class Participant(Base):
    __tablename__ = "participant"

    id = Column(Integer, primary_key = True)
    name = Column(String(256), nullable = False)
    individual = Column(Boolean, nullable = False)

class Election(Base):
    __tablename__ = "election"

    id = Column(Integer, primary_key = True)
    start = Column(DateTime, nullable = False)
    end = Column(DateTime, nullable = False)
    individual = Column(Boolean, nullable = False)

class Vote(Base):
    __tablename__ = "vote"

    id = Column(Integer, primary_key = True)
    uuid = Column(String(40), nullable = False)
    pollNumber = Column(Integer, nullable = False)
    valid = Column(Boolean, nullable = False)
    note = Column(String(50), nullable = False)
    electionid = Column(Integer, ForeignKey("election.id"), nullable = False)
    voterid = Column(String(13), nullable = False)


class ElectionParticipant(Base):
    __tablename__ = "electionparticipant"

    id = Column(Integer, primary_key = True)
    participantid = Column(Integer, ForeignKey("participant.id"), nullable = False)
    electionid = Column(Integer, ForeignKey("election.id"), nullable = False)
    pollNumber = Column(Integer, nullable = False)
