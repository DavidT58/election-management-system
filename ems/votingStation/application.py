from logging import debug
from flask import Flask, json, jsonify
from flask.globals import request
from flask.wrappers import Response
from configuration import Configuration
from flask_jwt_extended import JWTManager, get_jwt
from roleCheck import roleCheck
from redis import Redis


application = Flask(__name__)
application.config.from_object(Configuration)

jwt = JWTManager(application)

@application.route("/vote", methods = ["POST"])
@roleCheck(role = "official")
def vote():

    if request.data is None:
        return jsonify(message = 'Field file is missing.'), 400

    if 'file' not in request.files:
        return jsonify(message = 'Field file is missing.'), 400
    
    fileRequest = request.files['file'].readlines()

    votes = []

    for line in fileRequest:
        votes.append(line.decode('utf-8').split(','))

    for i in range(len(votes)):        
        if len(votes[i]) != 2:
            return jsonify(message = "Incorrect number of values on line {}.".format(i)), 400
        
        votes[i][0] = votes[i][0].strip()
        votes[i][1] = votes[i][1].strip()

        if not (votes[i][1]).isdigit():
            return jsonify(message = "Incorrect poll number on line {}.".format(i)), 400
        

    with Redis(host = Configuration.REDIS_HOST, port = 6379) as redis:
        for vote in votes:
            # wrap vote data into a JSON object and push it to redis
            voteData = {
                "id": vote[0],
                "pollNumber": vote[1],
                "voterJMBG": get_jwt()["jmbg"]
            }
            # redis.rpush(Configuration.REDIS_VOTE_QUEUE, json.dumps(voteData))
            redis.publish(Configuration.REDIS_VOTE_QUEUE, json.dumps(voteData))

    return Response(status=200)

if (__name__ == '__main__'):
    application.run(debug=True, host='0.0.0.0', port=80)
